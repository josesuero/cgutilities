package mstn.clearg.cache;

import java.io.IOException;
import java.net.InetSocketAddress;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.OperationTimeoutException;

public class Memcached {
	private int defaultTime = 3600; // una hora
	private int portNum = 11211; // Numero del puerto
	private String hostName = "localhost";
	private MemcachedClient c = null;
	private String prefix;

	public enum CacheLevel {
		Global, Schema, User
	}

	public static String concat(String... strs) {
		StringBuilder strbuild = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			if (strs[i] != null) {
				strbuild.append(strs[i]);
			}
		}
		return strbuild.toString();
	}

	public Memcached(String schema, String user, CacheLevel level)
			throws IOException,OperationTimeoutException {
		c = new MemcachedClient(new InetSocketAddress(hostName, portNum));
		switch (level) {
		case Global:
			prefix = "";
			break;
		case Schema:
			prefix = schema;
			break;
		case User:
			prefix = concat(schema, ".", user);
		}
	}

	public boolean put(String name, String value) {
		return put(name, value, this.defaultTime);
	}

	public boolean put(String name, String value, int time) {
		try {
			name = concat(prefix, name);
			if (c.set(name, time, value) != null) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;

	}

	public String get(String name) {
		name = concat(prefix, name);

		try {
			return (String) c.get(name);
		} catch (Exception e) {
			return null;
		}

	}

	public boolean remove(String name) {
		try {
			name = concat(prefix, name);
			if (c.delete(name) != null) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;

	}

	public boolean exists(String name) {
		name = concat(prefix, name);
		try{
		if (c.get(name) != null) {
			return true;
		}
		} catch (Exception e){
			return false;
		}
		return false;
	}
}
