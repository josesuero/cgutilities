package mstn.clearg.cache;

import net.sf.ehcache.*;
import net.sf.ehcache.config.*;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;
import net.sf.ehcache.store.*;

public class CGCache {
	private String prefix;

	private static Cache customCache;
	static {
		/*
		 * CacheManager cacheManager = CacheManager.getInstance();
		 */
		// Create a singleton CacheManager using defaults
		CacheManager cacheManager = CacheManager.create();

		// Create a Cache specifying its configuration.
		Cache testCache = new Cache(new CacheConfiguration("ClearG", 1000)
				.memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)
				.eternal(true)
				.timeToLiveSeconds(3600)
//				.timeToIdleSeconds(30)
				.diskExpiryThreadIntervalSeconds(0)
				.persistence(
						new PersistenceConfiguration()
								.strategy(Strategy.LOCALTEMPSWAP)));
		cacheManager.addCache(testCache);
		customCache = (Cache) cacheManager.getCache("ClearG");
	}

	public enum CacheLevel {
		Global, Schema, User
	}

	public static String concat(String... strs) {
		StringBuilder strbuild = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			if (strs[i] != null) {
				strbuild.append(strs[i]);
			}
		}
		return strbuild.toString();
	}

	public CGCache(String schema, String user, CacheLevel level) {
		switch (level) {
		case Global:
			prefix = "";
			break;
		case Schema:
			prefix = schema;
			break;
		case User:
			prefix = concat(schema, ".", user);
		}
	}

	public boolean put(String name, Object value) {
		return put(name, value, 0);
	}

	public boolean put(String name, Object value, int time) {
		try {
			name = concat(prefix, name);
			customCache.put(new Element(name, value));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Object get(String name) {
		name = concat(prefix, name);

		Element e = customCache.get(name);
		if (e != null)
			return e.getObjectValue();
		else
			return null;
	}

	public boolean remove(String name) {
		name = concat(prefix, name);
		customCache.remove(name);
		return false;
	}

	public boolean exists(String name) {
		name = concat(prefix, name);
		return customCache.isKeyInCache(name);
	}
	
	public Cache getCache(){
		return customCache;
	}
}
