package mstn.clearg.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import mstn.clearg.language.cgProcedure;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class cgRequest {
	private String token;
	private String uri;
	private String schema;
	
	public cgRequest(String schema, String token, String uri){
		
		this.token = token;
		this.uri = uri;
		this.schema = schema;
	}


	/*
    public String senddata(String command, String data) {
		String result = "";
		try {
			String datastr = (data !=null) ? URLEncoder.encode(data, "UTF-8") : "";
			String tokenstr = (this.token != null) ? URLEncoder.encode(this.token, "UTF-8"):"";

			URL url = new URL(this.uri);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);

			OutputStreamWriter out = new OutputStreamWriter(connection
					.getOutputStream());
			out.write("data=" + datastr);
			out.write("&token=" + tokenstr);
			out.write("&procedure=" + command);		
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			String decodedString;

			while ((decodedString = in.readLine()) != null) {
				result += decodedString;
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	*/
    
    public JSONObject senddata(String command, String data) throws JSONException{
    	return senddata(command, this.schema, data);
    }
    
    public JSONObject senddata(String command, String schema, String data) throws JSONException{
		String result = "";
		try {
			String datastr = (data !=null) ? URLEncoder.encode(data, "UTF-8") : "";
			String tokenstr = (this.token != null) ? URLEncoder.encode(this.token, "UTF-8"):"";
			String schemastr = (this.schema != null) ? URLEncoder.encode(this.schema, "UTF-8"):"";

			URL url = new URL(this.uri);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);

			OutputStreamWriter out = new OutputStreamWriter(connection
					.getOutputStream());
			out.write("data=" + datastr);
			out.write("&token=" + tokenstr);
			out.write("&schema=" + schemastr);
			out.write("&procedure=" + command);		
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			String decodedString;

			while ((decodedString = in.readLine()) != null) {
				result += decodedString;
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new JSONObject(result);
    	
    	
    }
    
	public Object getJsonData(String command, String data) throws Exception {
		return getJsonData(command, this.schema, data);
	}
	
	public Object getJsonData(String command, String schema, String data) throws Exception {
		JSONObject result = senddata(command, schema, data);
		
		if (result.getInt("status") == 200){
			return result.get("content");
		} else {
			throw new Exception(result.toString());
		}
	}


	@SuppressWarnings("unchecked")
	public String getScreen(String screenname) throws Exception {
		cgProcedure name  = new cgProcedure(screenname, this.schema);
		JSONObject screenDef = new JSONObject();
		screenDef.put("table", "cgapp.cgap_screens");
		screenDef.put("fields", "name, code");
		JSONArray filter = new JSONArray();
		filter.add(new JSONObject());
		filter.getJSONObject(0).put("field", "name");
		filter.getJSONObject(0).put("value", name.getName());
		screenDef.put("filter", filter);

		JSONObject screen;
		if (name.getSchema().isEmpty()){
			screen = (JSONObject)getJsonData("CLEARG.CG_GET", screenDef.toString());
		} else {
			screen = (JSONObject)getJsonData("CLEARG.CG_GET", name.getSchema(), screenDef.toString()); 
		}
		return screen.getJSONArray("data").getJSONObject(0).getString("CODE"); 
	}

	public Object toJSON(String data) throws JSONException{
		Object json;
		if (data.startsWith("{")) {
			json = new JSONObject(data);
		} else {
			json = new JSONArray(data);
		}
		return json;
	}

}