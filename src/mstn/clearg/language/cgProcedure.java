package mstn.clearg.language;

public class cgProcedure {
	private String name;
	private String schema;
	private String currentSchema;
	
	public cgProcedure(String name, String schema){
		this.name = name;
		this.currentSchema = schema;
		this.schema = "";
		if (name.indexOf(".") != -1){
			String[] names = name.split("\\.");
			if (names.length == 1){
				this.name = "";
				this.schema = names[0];
			} else {
				this.name = names[1];
				this.schema = names[0];
			}
		}

		if (schema.equals("") || this.schema.equals(schema)){
			this.schema = "";
		} else {
			this.currentSchema = this.schema;
		}
	}		
	
	 public String getName(){
		return this.name;
	}
	
	public String getSchema(){
		return this.schema;
	}
	
	public String getObjSchema(){
		if (this.schema.isEmpty()){
			return this.currentSchema;
		} else {
			return this.schema;
		}
	}
	
	public String getCurrentSchema(){
		return this.currentSchema;
	}	
	
	public String getFullName(){
		String fullName = this.schema;
		if (!this.schema.isEmpty()){
			fullName += ".";
		}
		return fullName + this.name;
	}

	public String toString(){
		return getFullName();
	}
}
